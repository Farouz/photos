<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('top_logo');
            $table->string('background_image');
            $table->integer('phone');
            $table->string('facebook')->default('#');
            $table->string('twitter')->default('#');
            $table->string('email');
            $table->string('instagram')->default('#');
            $table->string('youtube')->default('#');
            $table->string('pinterest')->default('#');
            $table->string('behance')->default('#');
            $table->string('vimo')->default('#');
            $table->string('company_profile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
