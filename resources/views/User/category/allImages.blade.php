@extends('User.Layout.master')
@section('CssFiles')
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/image-popup/source/jquery.fancybox.css?v=2.1.5')}}"
          media="screen">
    <link rel="stylesheet" type="text/css"
          href="{{asset('User/image-popup/source/helpers/jquery.fancybox-buttons.css?v=1.0.5')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/style-ar.css')}}">
    <style>
       .imgContainer {
            width: 100%;
            height: 200px;
        }
    </style>
@stop
@section('content')
    <div class="fixed-bg">
        <img src="{{asset('User/images/1.jpg')}}">
    </div>


    <div class="main-content">
        <div class="container-fluid">
            @if(\App::isLocale('en'))

                <h1 class="main-heading">{{$category->title_en}}</h1>
            @else
                <h1 class="main-heading">{{$category->title_ar}}</h1>
            @endif

            <div class="row">
                @foreach($images as $image)
                <div class="col-xs-12 col-sm-3 ">
                    <a class="fancybox-buttons img-holder small-img" rel="gallery" title="" data-fancybox-group="button"
                       href="{{asset('uploads/images/'.$image->image)}}">
                        <img src="{{asset('uploads/images/'.$image->image)}}" alt="img" class="imgContainer">
                    </a>
                </div>
                    @endforeach
            </div>

        </div>
    </div>




@stop

@section('scripts')
    <script src="{{asset('User/js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{asset('User/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('User/image-popup/source/jquery.fancybox.js?v=2.1.5')}}"></script>
    <script type="text/javascript" src="{{asset('User/image-popup/source/helpers/jquery.fancybox-buttons.js?v=1.0.5')}}"></script>
    <script>
        $(document).ready(function (){
            /*Button helper. Disable animations, hide close button, change title type and content*/

            $('.fancybox-buttons').fancybox({
                openEffect  : 'none',
                closeEffect : 'none',

                prevEffect : 'none',
                nextEffect : 'none',

                closeBtn  : false,

                helpers : {
                    title : {
                        type : 'inside'
                    },
                    buttons	: {}
                },

                afterLoad : function() {
                    this.title = '<a href="https://www.facebook.com/sharer.php?u={{asset('uploads/images/'.$category->image)}}" class="btn btn-fb btn-small"><i class="fa fa-facebook right-fa"></i> Share</a>' +
                        '<a href="https://www.twitter.com/sharer.php?u={{asset('uploads/images/'.$category->image)}}" class="btn btn-tw btn-small"><i class="fa fa-twitter right-fa"></i> Share</a>' +
                        '<a href="https://www.instagram.com/sharer.php?u={{asset('uploads/images/'.$category->image)}}" class="btn btn-inst btn-small"><i class="fa fa-instagram right-fa"></i> Share</a>';
                }

            });


        });
    </script>



    <script src="{{asset('User/js/script.js')}}"></script>
@stop