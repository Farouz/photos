@extends('User.Layout.master')
@section('CssFiles')
    <style>
        .imgContainer {
            width: 100%;
            height: 200px;
        }
    </style>
    @stop
@section('content')

    <link rel="stylesheet" href="{{asset('dist/css/bootstrap-rtl.min.css')}}">

    <div class="fixed-bg">
        <!-- Settting->backgorung_image -->
        <img src="{{asset('User/images/1.jpg')}}">
    </div>


    <div class="main-content">
        <div class="container-fluid">
            <h1 class="main-heading">@lang('alert.OurWorks')</h1>
            <div class="row">
                @foreach($categories as $category)
                <div class="col-xs-12 col-sm-4">
                    <a href="/gallery/{{$category->id}}" class="img-holder">
                        <img src="{{asset('uploads/images/'.$category->image)}}" alt="..." class="imgContainer">

                        <div class="hover-content">
                            @if(\App::isLocale('en'))
                            <h1>{{$category->title_en}}</h1>
                                @else
                                <h1>{{$category->title_ar}}</h1>
                                @endif
                        </div>
                    </a>
                </div>
                    @endforeach
            </div>
        </div>
    </div>
@stop
