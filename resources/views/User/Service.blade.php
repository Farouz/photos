@extends('User.Layout.master')
@section('content')
        <!-- Making the words from right to left      -->

        <link rel="stylesheet" href="{{asset('dist/css/bootstrap-rtl.min.css')}}">
        <div class="fixed-bg">
@foreach($settings as $setting)

        <img src="{{asset('uploads/images/'.$setting->background_image)}}">

@endforeach
</div>
        <div class="main-content">
            <div class="container">
                <h1 class="main-heading">@lang('alert.OurServices')</h1>

                @foreach($settings as $setting)
                    <div class="border-bottom">
                        @if(\App::islocale('ar'))
                            <p> {{strip_tags($setting->service_ar)}}</p>
                        @else
                            <p>{{$setting->service_en}}</p>

                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    @stop