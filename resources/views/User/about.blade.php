@extends('User.Layout.master')
@section('content')

    <div class="fixed-bg">
        @foreach($settings as $setting)
        <img src="{{asset('uploads/images/'.$setting->background_image)}}">
            @endforeach
    </div>

    <div class="main-content">
        <div class="container">
            <h1 class="main-heading">@lang('alert.whoAreWe')</h1>

            <div class="text-center div-padding">
                @foreach($settings as $setting)
                    @if(\App::islocale('en'))
                    <p>{{strip_tags($setting->summary_en)}}</p>
                    @else
                        <p>{{strip_tags($setting->summary_ar)}}</p>
                    @endif
                @endforeach

                @foreach($settings as $setting)
                <a href="{{asset('uploads/Files/'.$setting->company_profile)}}" target="_blank" class="btn btn-white margin"><span>@lang('alert.download')</span></a>
                <a href="{{route('GET_GALLERY')}}" class="btn btn-white margin"><span>@lang('alert.showWorks')</span></a>
                @endforeach
            </div>


            <div class="div-small-padding">
                <h1 class="main-heading">@lang('alert.OurSponsors')</h1>
                <div class="row">
                    <div class="col-xs-2 col-sm-1 no-padding text-center">
                        <a class="owl-btn prev-pro margin"><span class="fa fa-angle-right"></span></a>
                    </div>

                    <div class="col-xs-8 col-sm-10 no-padding">
                        <div id="owl-demo-products" class="owl-carousel-clients">
                            @foreach($customers as $customer)

                            <div class="item">
                                <a class="fancybox-buttons" data-fancybox-group="button" href="{{asset('uploads/images/'.$customer->image)}}">
                                    <img src="{{asset('uploads/images/'.$customer->image)}}" alt="img">
                                </a>
                            </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="col-xs-2 col-sm-1 no-padding text-center">
                        <a class="owl-btn next-pro margin"><span class="fa fa-angle-left"></span></a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @stop