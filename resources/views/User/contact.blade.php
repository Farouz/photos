@extends('User.Layout.master')
@section('CssFiles')
    <style>
        input[type="file"] {
            padding: 0;
        }

        .black-box.margin-bottom {
            margin: 0 0 15px;
        }

        .checkbox-holder {
            font-weight: 100;
            position: relative;
            cursor: pointer;
            margin-bottom: 10px;
            display: block;
        }

        .checkbox-holder span {
            vertical-align: middle;
        }

        .checkbox-holder .checkbox-icon {
            width: 13px;
            height: 13px;
            line-height: 7px;
            display: inline-block;
            border: 1px solid #000;
            background: #000;
            text-align: center;
            margin: 0 4px;
        }

        .checkbox-holder input[type="checkbox"] {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }

        .checkbox-holder .checkbox-icon:after {
            content: '';
            background: #000;
            width: 7px;
            height: 7px;
            display: block;
            margin: 2px;
        }

        .checkbox-holder input[type="checkbox"]:checked + .checkbox-icon {
            border-color: #00bcd4;
        }

        .checkbox-holder input[type="checkbox"]:checked + .checkbox-icon:after {
            background: #00bcd4;
        }

        .main-label {
            border-bottom: 1px dashed #00bcd4;
        }

        .check-open {
            margin-top: 10px;
        }
    </style>
@stop
@section('content')
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/style-ar.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/bootstrap-rtl.min.css')}}">
    <div class="fixed-bg">
        @foreach($settings as $setting)
        <img src="{{asset('uploads/images/'.$setting->background_image)}}">
            @endforeach
    </div>


    <div class="main-content">
        <div class="container">
            <h1 class="main-heading">@lang('alert.ContactUs')</h1>

            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <form method="post" action="{{route('POST_USER_ORDER')}}" ENCTYPE="multipart/form-data">
                        {{csrf_field()}}
                        <input type="text" name="name" placeholder="@lang('alert.name@form')">
                        <input type="text" name="activity" placeholder="@lang('alert.activity') ">
                        <input type="tel" name="phone" placeholder="@lang('alert.Phone')">
                        <input type="email" name="email" placeholder="@lang('alert.YourMail')">


                        <label>@lang('alert.Service')</label>
                        <div class="row">
                        @foreach($packages as $package)

                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="box black-box margin-bottom">


                                        <div class="main-label">
                                            <label class="checkbox-holder">
                                                <input type="checkbox" name="packages[]" value="{{$package->id}}">
                                                <span class="checkbox-icon"></span>
                                                <span>{{$package->title_en}}</span>
                                            </label>
                                        </div>


                                        <div class="check-open">
                                            <?php
                                            $subs = \App\SubPackage::where('package_id', $package->id)->get();

                                            ?>

                                            @if(count($subs) > 0 )
                                                    <div class="row">
                                                @foreach($subs as $sub_package)
                                                    <label class="checkbox-holder">
                                                        <input type="checkbox" name="sub_packs[{{$package->id}}][]"
                                                               value="{{ $sub_package->id }}">
                                                        <span class="checkbox-icon"></span>
                                                        <span>{{$sub_package->title_en}}</span>
                                                    </label>
                                                @endforeach
                                                    </div>
                                            @endif
                                            <br>
                                            <label for="sub_packs_else">@lang('alert.Other')</label>
                                            <input name="sub_packs_else[{{$package->id}}]" type="text" id="sub_packs_else" placeholder="@lang('alert.Other')">

                                            <label for="photos_numbers">@lang('alert.ImageNumber')</label>
                                            <input id="photos_numbers" type="text" name="photos_numbers[{{$package->id}}]">
                                            <?php
                                            $types = \App\PhotoType::where('package_id', $package->id)->get();
                                            ?>
                                            @if(count($types) > 0)
                                                @foreach($types as $type)
                                                    <label class="checkbox-holder">
                                                        <input type="checkbox" value="{{$type->id}}" name="order_image[]">
                                                        <span class="checkbox-icon"></span>
                                                        <span>{{$type->title_en}}</span>
                                                    </label>
                                                @endforeach
                                            @endif

                                        </div>
                                    </div>
                                </div>

                        @endforeach
                        </div>


                        <label>@lang('alert.AttachFile')</label>
                        <input type="file" name="order_file" placeholder="@lang('alert.AttachFile')">

                        <div class="btn btn-white">
                            <input type="submit" value="@lang('alert.Send')">
                        </div>
                    </form>
                    @if(count($errors))
                        <div class="form-group" >
                            <div class="alert alert-error">
                                <ul>
                                    @foreach($errors->all() as $error )
                                        <li> {{$error}} </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="box black-box text-center">
                        <h3 class="main-heading">@lang('alert.ContactDetails')</h3>
                            @foreach($settings as $setting)
                        <p><i class="fa fa-envelope-o right-fa"></i> {{$setting->email}}</p>
                        <p><i class="fa fa-phone right-fa"></i> {{$setting->phone}}</p>
                                @endforeach
                    </div>
                    <div class="box black-box text-center">
                        <h3 class="main-heading">@lang('alert.joinUS')</h3>

                        <form method="post" action="{{route('POST_SUBSCRIBER')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="email" name="email" placeholder="@lang('alert.YourMail')">
                            <div class="btn btn-white btn-block">
                                <input type="submit" value="@lang('alert.Send')">
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src="{{asset('User/js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{asset('User/js/bootstrap.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.check-open').slideUp(0);

            $('.main-label .checkbox-holder').click(function () {
                if ($(this).find('input').is(':checked')) {
                    $(this).parents('.main-label').next('.check-open').stop().slideDown();
                } else {
                    $(this).parents('.main-label').next('.check-open').stop().slideUp();
                }
            });
        });
    </script>
    <script src="{{asset('User/js/script.js')}}"></script>
@stop