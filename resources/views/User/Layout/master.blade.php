<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <title>Photo Maker</title>
    <link rel="icon" type="image/png" href="{{asset('User/images/icon.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/style.css')}}">
    @if(\App::islocal('en'))
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/style-en.css')}}">
    @else
    <link rel="stylesheet" type="text/css" href="{{asset('User/css/style-ar.css')}}">
    @endif
    @yield('CssFiles')

</head>

<body>

<!--===============================
    NAV
===================================-->

<nav class="navbar navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                    <span class="fa fa-bars"></span>
                </button>

                <a href="{{route('Home')}}" class="navbar-brand hidden-sm hidden-md hidden-lg"><img src="{{asset('User/images/logo.png')}}" alt="LOGO"></a>
            </div>

            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right text-align-left">
                    <li class="active"><a href="{{route('Home')}}">@lang('alert.Home')</a></li>
                    <li><a href="{{route('About')}}">@lang('alert.About')</a></li>
                    <li><a href="{{route('Services')}}">@lang('alert.OurServices')</a></li>
                </ul>

                <a href="{{route('Home')}}" class="navbar-brand hidden-xs text-center"><img src="{{asset('User/images/logo.png')}}" alt="LOGO"></a>

                <ul class="nav navbar-nav navbar-left text-align-right">
                    <li><a href="{{route('GET_GALLERY')}}">@lang('alert.Gallery')</a></li>
                    <li><a href="{{route('GET_CONTACT')}}">@lang('alert.Contact')</a></li>
                    @if(\App::isLocale('ar'))
                        <li><a  href="{{route('CHANGE_LANGUAGE','en')}}">English</a></li>
                    @else
                        <li><a  href="{{route('CHANGE_LANGUAGE','ar')}}">عربـي</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</nav>


<section class="container">

    @yield('content')
</section>

<!--===============================
    FOOTER
===================================-->

<footer class="navbar-fixed-bottom text-center">
    <div class="container">

        <p> جميع الحقوق محفوظة لمؤسسة Ieasoft  &copy; 2017-2018</p>

        @foreach($settings as $setting)
            <a href="{{$setting->facebook}}"><i class="fa fa-facebook"></i></a>
            <a href="{{$setting->twitter}}"><i class="fa fa-twitter"></i></a>
            <a href="{{$setting->instagram}}"><i class="fa fa-instagram"></i></a>
            <a href="{{$setting->youtube}}"><i class="fa fa-youtube"></i></a>
            <a href="{{$setting->pinterest}}"><i class="fa fa-pinterest"></i></a>
            <a href="{{$setting->behance}}"><i class="fa fa-behance"></i></a>
            <a href="{{$setting->vimeo}}"><i class="fa fa-vimeo"></i></a>
        @endforeach
    </div>
</footer>


<!--===============================
    SCRIPT
===================================-->

<script src="{{asset('User/js/jquery-1.11.1.min.js')}}"></script>
<script src="{{asset('User/js/bootstrap.min.js')}}"></script>
<script src="{{asset('User/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('User/js/script.js')}}"></script>
@yield('scripts')
</body>
</html>