@extends('dashboard.layout.master')
<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
@section('content')
    <h2>@lang('alert.AllPackage')</h2>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h2>@lang('alert.AllPackage')</h2>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th> Name</th>
                        <th>الاسـم</th>
                        <th>Package</th>
                        <th>الباكدج</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Control</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subPackages as $subPackage)
                        <tr>
                            <td>{{$subPackage->id}}</td>
                            <td>{{$subPackage->title_en}}</td>
                            <td>{{$subPackage->title_ar}}</td>
                            <td>{{\App\Package::where('id',$subPackage->package_id)->first()->title_en}}</td>
                            <td>{{\App\Package::where('id',$subPackage->package_id)->first()->title_ar}}</td>
                            <td>{{$subPackage->updated_at}}</td>
                            <td>{{$subPackage->created_at}}</td>
                            <td>
                                <a href="{{route('GET_DELETE_SUB_PACKAGE',$subPackage->id)}}"><i class="fa fa-trash fa-lg" data-toggle="tooltip" title="Delete "></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>
            </div><!-- /.box-body -->
        </div>
    </section>

@stop
@section('DashScripts')
    <script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

@stop