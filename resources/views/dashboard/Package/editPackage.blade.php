@extends('dashboard.layout.master')
@section('content')
    <div>
        <h2>@lang('alert.EditImage')</h2>
        <form action="{{route('POST_EDIT_PACKAGE',$package->id)}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="title_en">Package title</label>
                    <input type="text" class="form-control" name="title_en" id="title_en" value="{{$package->title_en}}">
                </div>
                <div class="form-group">
                    <label for="title_ar">الأسم </label>
                    <input type="text" class="form-control" id="title_ar" name="title_ar" value="{{$package->title_ar}}">
                </div>
                <div class="form-group">
                    <input type="submit" value="@lang('alert.Edit')">
                </div>
            </div>

        </form>
    </div>
@stop