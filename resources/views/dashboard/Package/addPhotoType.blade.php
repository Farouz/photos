@extends('dashboard.layout.master')
@section('content')
    <div>
        <h2>@lang('alert.AddPhotoType')</h2>
        <form action="{{route('POST_ADD_PHOTO_TYPE')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="package_id" value="{{$package->id}}">
            <div class="box-body">
                <div class="form-group">
                    <label for="title_en"> title</label>
                    <input type="text" class="form-control" name="title_en" id="title_en" placeholder="Package Title.." required>
                </div>
                <div class="form-group">
                    <label for="title_ar">الأسم </label>
                    <input type="text" class="form-control" id="title_ar" name="title_ar" placeholder="اسم الخدمه" required>
                </div>
                <div class="form-group">
                    <input type="submit" value="@lang('alert.Add')">
                </div>
            </div>

        </form>
    </div>

@stop