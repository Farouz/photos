@extends('dashboard.layout.master')
@section('content')
<h3>Send Mail TO : {{$order->name}}</h3>
<h4>Email : {{$order->email}}</h4>
<h4>Order Details :  </h4>
<h4>Package : </h4> <p>{{\App\Package::where('id',$order->service)->first()->title_en}}</p>
<h4>Services : </h4> <p> @if(!empty($order->service))
        <?php
        $order_service_arr=explode(" ",$order->service)
        ?>
        @foreach($order_service_arr as $service)
            <?php
            $or_service=\App\SubPackage::where('id',$service)->first();
            ?>
            @if(!empty($or_service))
                {{$or_service['title_'.App::getLocale()]}} ,
            @endif
        @endforeach
    @endif</p>

<h4>Images Types : </h4> <p>
    @if(!empty($order->order_image))
        <?php
        $order_images_arr = explode(" ", $order->order_image);
        ?>
        @foreach ($order_images_arr as $img)
            <?php
            $or_img = \App\PhotoType::where('id', $img)->first();

            ?>

            @if(!empty($or_img))
                {{ $or_img['title_'.App::getLocale()] }} ,
            @endif

        @endforeach
    @endif</p>
<h4>Photos Numbered Ordered : </h4> <p>{{$order->photos_numbers}}</p>
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<form method="post" action="{{route('POST_SEND_MAIL')}}" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="hidden" name="email" value="{{$order->email}}">
    <textarea name="title"></textarea>
    <script>
        CKEDITOR.replace( 'title' );
    </script>
    <input type="submit" value="Send">
</form>

    @stop