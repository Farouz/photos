@extends('dashboard.layout.master')
<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
@section('content')
    <h2>@lang('alert.AllOrders')</h2>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">@lang('alert.AllOrders')</h3>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Activity</th>
                        <th>Phone</th>
                        <td>Other</td>
                        <th>Package</th>
                        <th>Service</th>

                        <th>Images Type</th>
                        <th>Images Numbers</th>
                        <th>Created At</th>
                        <th>Control</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->name}}</td>
                            <td>{{$order->email}}</td>
                            <td>{{$order->activity}}</td>
                            <td>{{$order->phone}}</td>
                            <td>{{$order->sub_packs_else}}</td>
                            <td> {{\App\Package::where('id',$order->service)->first()->title_en}}</td>
                            <td>
                                @if(!empty($order->service))
                                    <?php
                                    $order_service_arr=explode(" ",$order->service)
                                    ?>
                                    @foreach($order_service_arr as $service)
                                        <?php
                                        $or_service=\App\SubPackage::where('id',$service)->first();
                                        ?>
                                        @if(!empty($or_service))
                                            {{$or_service['title_'.App::getLocale()]}} ,
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td>


                                @if(!empty($order->order_image))
                                    <?php
                                    $order_images_arr = explode(" ", $order->order_image);
                                    ?>
                                    @foreach ($order_images_arr as $img)
                                        <?php
                                        $or_img = \App\PhotoType::where('id', $img)->first();

                                        ?>

                                        @if(!empty($or_img))
                                            {{ $or_img['title_'.App::getLocale()] }} ,
                                        @endif

                                    @endforeach
                                @endif
                            </td>
                            <td>{{$order->photos_numbers}}</td>
                            <td>{{$order->created_at}}</td>
                            <td>
                                <a href="{{route('GET_DELETE_ORDER',$order->id)}}"><i class="fa fa-trash fa-lg" data-toggle="tooltip" title="Delete "></i></a> &#8209;
                                <a href="{{route('GET_SEND_MAIL',$order->id)}}"><i class="fa fa-send fa-lg" data-toggle="tooltip" title="Reply "></i></a>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>
            </div><!-- /.box-body -->
        </div>
    </section>

@stop
@section('DashScripts')
    <script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

@stop