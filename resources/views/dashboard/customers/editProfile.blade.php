@extends('dashboard.layout.master')
@section('content')
    <div>
        <h2>@lang('alert.EditCustomer')</h2>
        <form action="{{route('POST_EDIT_CUSTOMER',$customer->id)}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="title_en">Customer title</label>
                    <input type="text" class="form-control" name="title_en" id="title_en" value="{{$customer->title_en}}">
                </div>
                <div class="form-group">
                    <label for="title_ar">أسم العميل </label>
                    <input type="text" class="form-control" id="title_ar" name="title_ar" value="{{$customer->title_ar}}">
                </div>
                <div class="form-group">
                    <label for="image">Category Image</label>
                    <img src="{{asset('uploads/images/'.$customer->image)}}" style="width: 180px ;height:180;">
                    <input type="file" id="image" name="image">
                </div>
                <div class="form-group">
                    <input type="submit" value="@lang('alert.Edit')">
                </div>
            </div>

        </form>
    </div>

@stop