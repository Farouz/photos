@extends('dashboard.layout.master')
@section('content')
    <div>
        <h2>@lang('alert.AddCustomer')</h2>
        <form action="{{route('POST_ADD_CUSTOMER')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="title_en">Customer Name</label>
                    <input type="text" class="form-control" name="title_en" id="title_en" placeholder="Customer Title.." required>
                </div>
                <div class="form-group">
                    <label for="title_ar">أسم العميل </label>
                    <input type="text" class="form-control" id="title_ar" name="title_ar" placeholder="اسم العميل" required>
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" id="image" name="image">
                </div>
                <div class="form-group">
                    <input type="submit" value="@lang('alert.Add')">
                </div>
            </div>

        </form>
    </div>



@stop