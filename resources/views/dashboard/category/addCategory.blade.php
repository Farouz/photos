@extends('dashboard.layout.master')
@section('DashCssFiles')

    @stop
@section('content')
    <div>
        <h2>@lang('alert.AddCategory')</h2>
        <form action="{{route('POST_ADD_CATEGORY')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="title_en">Category title</label>
                    <input type="text" class="form-control" name="title_en" id="title_en" placeholder="Category Title..">
                </div>
                <div class="form-group">
                    <label for="title_ar">أسم القسم </label>
                    <input type="text" class="form-control" id="title_ar" name="title_ar" placeholder="اسم القسم">
                </div>
                <div class="form-group">
                    <label for="image">Category Image</label>
                    <input type="file" id="image" name="image">
                </div>
                <div class="form-group">
                    <input type="submit" value="@lang('alert.Add')">
                </div>
            </div>
        </form>
    </div>



@endsection