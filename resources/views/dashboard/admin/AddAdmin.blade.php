@extends('dashboard.layout.master')
@section('content')
    <div>
        <h2>Add Admin</h2>
        <form action="{{route('POST_ADD_ADMIN')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group">
                    <input type="submit" value="ADD">
                </div>
            </div>

        </form>
    </div>
    @if(count($errors))
        <div class="form-group" >
            <div class="alert alert-error">
                <ul>
                    @foreach($errors->all() as $error )
                        <li> {{$error}} </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif


@stop