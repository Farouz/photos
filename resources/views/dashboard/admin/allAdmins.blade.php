@extends('dashboard.layout.master')
@section('content')
    <h2>All Admins</h2>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Admins</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email </th>
                                <th>Control</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($admins as $admin)
                                <tr>
                                    <td>{{$image->id}}</td>
                                    <td>{{$image->name}}</td>
                                    <td>{{$image->email}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success dropdown-toggle"
                                                    data-toggle="dropdown" aria-expanded="false"> Actions
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">

                                                <li>
                                                    <a href="{{route('GET_DELETE_ADMIN',$admin->id)}}">
                                                        Delete Admin
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop