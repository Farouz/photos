@extends('dashboard.layout.master')
@section('content')
    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
    <div>
        <h2>@lang('alert.setupSettings')</h2>
        <form action="{{route('POST_ADD_SETTING')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="company_name_en">Company Name</label>
                    <input type="text" class="form-control" name="company_name_en" id="company_name_en"
                           placeholder="Company Name" required>
                </div>
                <div class="form-group">
                    <label for="company_name_ar">أسم الشركه </label>
                    <input type="text" class="form-control" id="company_name_ar" name="company_name_ar"
                           placeholder="اسم الشركه" required>
                </div>
                <div class="form-group">
                    <label for="top_logo">Company Logo</label>
                    <span class="btn btn-success btn-file">

                         Browse <input type="file" id="top_logo" name="top_logo" required>
                    </span>
                </div>
                <div class="form-group">
                    <label for="background_image">Background Image</label>
                    <span class="btn btn-twitter btn-file">
                   Browse <input type="file" id="background_image" name="background_image" required>
                    </span>
                </div>
                <div class="form-group">
                    <label for="sliderBackground">Slider Background Images</label>
                    <span class="btn btn-twitter btn-file">
                   Browse <input type="file" id="slider" name="sliderBackground[]" required multiple>
                    </span>
                </div>
                <div class="form-group">
                    <label for="company_profile">Company profile File</label>
                    <span class="btn btn-facebook btn-file">
                    Upload<input type="file" id="company_profile" name="company_profile" required>
                        </span>
                </div>
                <hr>
                <h3> Contact Information</h3>
                <div class="form-group">
                    <label for="email">Company Email </label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <label for="phone">Company Phone </label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" required>
                </div>
                <hr>
                <h3> Social Media Accounts</h3>
                <div class="form-group">
                    <label for="facebook">FaceBook Account</label>
                    <input type="text" class="form-control" id="facebook" name="facebook"
                           placeholder="FaceBook Account" required>
                </div>
                <div class="form-group">
                    <label for="twitter">Twitter Account</label>
                    <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter Account" required>
                </div>
                <div class="form-group">
                    <label for="instagram">Instagram Account</label>
                    <input type="text" class="form-control" id="instagram" name="instagram"
                           placeholder="Instagram Account" required>
                </div>
                <div class="form-group">
                    <label for="youtube">YouTube Account</label>
                    <input type="text" class="form-control" id="youtube" name="youtube" placeholder="YouTube Account" required>
                </div>
                <div class="form-group">
                    <label for="pinterest">Pinterest Account</label>
                    <input type="text" class="form-control" id="pinterest" name="pinterest"
                           placeholder="Pinterest Account" required>
                </div>
                <div class="form-group">
                    <label for="behance">Behance Account</label>
                    <input type="text" class="form-control" id="behance" name="behance" placeholder="Behance Account" required>
                </div>
                <div class="form-group">
                    <label for="vimo">Vimo Account</label>
                    <input type="text" class="form-control" id="vimo" name="vimo" placeholder="Vimo Account" required>
                </div>
                <div class="form-group">
                    <label for="service_en">Our Service</label>
                    <textarea name="service_en"></textarea>
                    <script>
                        CKEDITOR.replace( 'service_en' );
                    </script>
                </div>
            <div class="form-group">
                    <label for="service_ar"><strong>خدماتنا</strong></label>
                    <textarea name="service_ar"></textarea>
                    <script>
                        CKEDITOR.replace( 'service_ar' );
                    </script>
                </div>
                <div class="form-group">
                    <label for="summary_en"><strong>Summary</strong></label>
                    <textarea name="summary_en"></textarea>
                    <script>
                        CKEDITOR.replace( 'summary_en' );
                    </script>
                </div>
                    <div class="form-group">
                    <label for="summary_ar"><strong>نبذه عن الشركه</strong></label>
                    <textarea name="summary_ar"></textarea>
                    <script>
                        CKEDITOR.replace( 'summary_ar' );
                    </script>
                </div>


                <div class="form-group">
                    <input type="submit" value="@lang('alert.Add')">
                </div>
            </div>

        </form>
    </div>



@stop