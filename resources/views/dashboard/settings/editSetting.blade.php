@extends('dashboard.layout.master')
@section('content')
    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

    <div>
        <h2>@lang('alert.setupSettings')</h2>
        <form action="{{route('POST_EDIT_SETTING',$setting->id)}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group">
                    <label for="company_name_en">Company Name</label>
                    <input type="text" class="form-control" name="company_name_en" id="company_name_en"
                           placeholder="Company Name" value="{{$setting->company_name_en}}">
                </div>
                <div class="form-group">
                    <label for="company_name_ar">أسم الشركه </label>
                    <input type="text" class="form-control" id="company_name_ar" name="company_name_ar"
                           placeholder="اسم الشركه" value="{{$setting->company_name_ar}}">
                </div>
                <div class="form-group">
                    <label for="top_logo">Company Logo</label>
                    <span class="btn btn-success btn-file">
                         Browse <input type="file" id="top_logo" name="top_logo" value="{{$setting->top_logo}}">
                    </span>
                </div>
                <div class="form-group">
                    <label for="background_image">Background Image</label>
                    <span class="btn btn-twitter btn-file">
                   Browse <input type="file" id="background_image" name="background_image"
                                 value="{{$setting->background_image}}">
                    </span>
                </div>
                <div class="form-group">
                    <label for="sliderBackground">Slider Background Images</label>
                    <span class="btn btn-twitter btn-file">
                   Browse <input type="file" id="slider" name="sliderBackground[]"
                                 value="{{$setting->SliderBackground}}" multiple>
                    </span>
                </div>
                <div class="form-group">
                    <label for="company_profile">Company profile File</label>
                    <span class="btn btn-facebook btn-file">
                    Upload<input type="file" id="company_profile" name="company_profile"
                                 value="{{$setting->company_profile}}">
                        </span>
                </div>
                <hr>
                <h3> Contact Information</h3>
                <div class="form-group">
                    <label for="email">Company Email </label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                           value="{{$setting->email}}">
                </div>
                <div class="form-group">
                    <label for="phone">Company Phone </label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number"
                           value="{{$setting->phone}}">
                </div>
                <hr>
                <h3> Social Media Accounts</h3>
                <div class="form-group">
                    <label for="facebook">FaceBook Account</label>
                    <input type="text" class="form-control" id="facebook" name="facebook"
                           placeholder="FaceBook Account" value="{{$setting->facebook}}">
                </div>
                <div class="form-group">
                    <label for="twitter">Twitter Account</label>
                    <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter Account"
                           value="{{$setting->twitter}}">
                </div>
                <div class="form-group">
                    <label for="instagram">Instagram Account</label>
                    <input type="text" class="form-control" id="instagram" name="instagram"
                           placeholder="Instagram Account" value="{{$setting->instagram}}">
                </div>
                <div class="form-group">
                    <label for="youtube">YouTube Account</label>
                    <input type="text" class="form-control" id="youtube" name="youtube" placeholder="YouTube Account"
                           value="{{$setting->youtube}}">
                </div>
                <div class="form-group">
                    <label for="pinterest">Pinterest Account</label>
                    <input type="text" class="form-control" id="pinterest" name="pinterest"
                           placeholder="Pinterest Account" value="{{$setting->pinterest}}">
                </div>
                <div class="form-group">
                    <label for="behance">Behance Account</label>
                    <input type="text" class="form-control" id="behance" name="behance" placeholder="Behance Account"
                           value="{{$setting->behance}}">
                </div>
                <div class="form-group">
                    <label for="vimo">Vimo Account</label>
                    <input type="text" class="form-control" id="vimo" name="vimo" placeholder="Vimo Account" value="{{$setting->vimo}}">
                </div>
                <div class="form-group">
                    <label for="service_en">Our Service</label>
                    <textarea name="service_en">{{$setting->service_en}}</textarea>
                    <script>

                        CKEDITOR.replace('service_en');
                    </script>
                </div>
                <div class="form-group">
                    <label for="service_ar"><strong>خدماتنا</strong></label>
                    <textarea name="service_ar">{{$setting->service_ar}}</textarea>
                    <script>
                        CKEDITOR.replace('service_ar');
                    </script>
                </div>
                <div class="form-group">
                    <label for="summary_en"><strong>Summary</strong></label>
                    <textarea name="summary_en">{{$setting->summary_en}}</textarea>
                    <script>
                        CKEDITOR.replace('summary_en');
                    </script>
                </div>
                <div class="form-group">
                    <label for="summary_ar"><strong>نبذه عن الشركه</strong></label>
                    <textarea name="summary_ar">{{$setting->summary_ar}}</textarea>
                    <script>
                        CKEDITOR.replace('summary_ar');
                    </script>
                </div>


                <div class="form-group">
                    <input type="submit" value="@lang('alert.Edit')">
                </div>
            </div>
        </form>
    </div>



@stop