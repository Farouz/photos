@extends('dashboard.layout.master')
<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
@section('content')
    <h2>@lang('alert.settings')</h2>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h2>@lang('alert.settings')</h2>
            </div>

            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>

                        <th> Title</th>
                        <th>الأسم</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Facebook Account</th>
                        <th>Twitter Account</th>
                        <th>Instagram Account</th>
                        <th>YouTube Account</th>
                        <th>Pinterest Account</th>
                        <th>Behance Account</th>
                        <th>Vimo Account</th>
                        <th> Created At</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($settings as $setting)
                        <tr>
                            <td>{{$setting->company_name_en}}</td>
                            <td>{{$setting->company_name_ar}}</td>
                            <td>{{$setting->email}}</td>
                            <td>{{$setting->phone}}</td>
                            <td>{{$setting->facebook}}</td>
                            <td>{{$setting->twitter}}</td>
                            <td>{{$setting->instagram}}</td>
                            <td>{{$setting->youtube}}</td>
                            <td>{{$setting->pinterest}}</td>
                            <td>{{$setting->behance}}</td>
                            <td>{{$setting->vimo}}</td>
                            <td>{{$setting->created_at}}</td>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>

                </table>
            </div><!-- /.box-body -->
            <hr>
            @foreach($settings as $setting)
                <label> Company Logo :</label>
                <div class="timeline-body">
                    <img src="{{asset('uploads/images/'.$setting->top_logo)}}" alt="..." class="margin">
                </div>
                <hr>
                <label> Website Background :</label>
                <div class="timeline-body">
                    <img src="{{asset('uploads/images/'.$setting->background_image)}}" alt="..." class="img-responsive">
                </div>
                <hr>
                <label> Website Sliders :</label>
                <div class="row">
                    @foreach(explode(" ",$setting->SliderBackground) as $slider)
                        <img src="{{asset('uploads/images/'.$slider)}}" alt="..." style="width: 200px; height: 159px" class="img-holder">
                    @endforeach
                </div>
            @endforeach
            <hr> <br>
            @foreach($settings as $setting)
            <div class="timeline body">
                <label>Service</label>
                <pre class="prettyprint"><code class="lang-html">{{$setting->service_en}}</code></pre>
            </div> <br>
                 <div class="timeline body">
                <label>خدماتنا</label>
                <pre class="prettyprint"><code class="lang-html">{{$setting->service_ar}}</code></pre>
            </div> <br>
                 <div class="timeline body">
                <label>Summary</label>
                <pre class="prettyprint"><code class="lang-html">{{$setting->summary_en}}</code></pre>
            </div> <br>
                 <div class="timeline body">
                <label>نبذه عن الشركه</label>
                <pre class="prettyprint"><code class="lang-html">{{$setting->summary_ar}}</code></pre>
            </div> <br>
               <a href="{{route('GET_EDIT_SETTING',$setting->id)}}"> <button type="button" class="btn btn-success btn-lg btn-block">Edit Settings</button></a>

            @endforeach
        </div>
    </section>

@stop
@section('DashScripts')
    <script src="{{asset('plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/app.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>

@stop