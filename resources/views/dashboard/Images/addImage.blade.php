@extends('dashboard.layout.master')
@section('content')
    <div>
        <h2>@lang('alert.AddImage')</h2>
        <form action="{{route('POST_ADD_Image')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" value="{{$category->id}}" name="cat_id">
            <input type="hidden" value="{{\Auth::user()->id}}" name="user_id">
            <div class="box-body">
                <div class="form-group">
                    <label for="title_en">Image title</label>
                    <input type="text" class="form-control" name="title_en" id="title_en" placeholder="Image Title..">
                </div>
                <div class="form-group">
                    <label for="title_ar">أسم الصوره </label>
                    <input type="text" class="form-control" id="title_ar" name="title_ar" placeholder="اسم الصوره">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" id="image" name="image">
                </div>
                <div class="form-group">
                    <input type="submit" value="@lang('alert.Add')">
                </div>
            </div>

        </form>
    </div>


    @stop