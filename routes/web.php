<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'dashboard'], function () {

    Route::get('/login', ['uses' => 'UsersController@getLogin', 'as' => 'GET_ADMIN_LOGIN']);
    Route::post('/login', ['uses' => 'UsersController@postLogin', 'as' => 'POST_ADMIN_LOGIN']);
    Route::group(['middleware' => 'isAuth'], function () {
        Route::get('/', ['uses' => 'UsersController@getDashboard', 'as' => 'GET_ADMIN_DASHBOARD']);
        Route::get('/signOut', ['uses' => 'UsersController@getSignOut', 'as' => 'GET_ADMIN_SIGN_OUT']);
        Route::get('/all-admins', ['uses' => 'UsersController@getAllAdmins', 'as' => 'GET_ALL_ADMINS']);
        Route::get('/deleteAdmin/{id}', ['uses' => 'UsersController@deleteAdmin', 'as' => 'GET_DELETE_ADMIN']);
        Route::get('/getAddAdmin', ['uses' => 'UsersController@getAddAdmin', 'as' => 'GET_ADD_ADMIN']);
        Route::post('/AddAdmin', ['uses' => 'UsersController@postAddAdmin', 'as' => 'POST_ADD_ADMIN']);
        Route::get('/add-category', ['uses' => 'CategoriesController@getAddCategory', 'as' => 'ADD_ADMIN_CATEGORY']);
        Route::get('/show-category/{id}', ['uses' => 'CategoriesController@getShowCategory', 'as' => 'GET_SHOW_SINGLE_CATEGORY']);
        Route::post('/add-category', ['uses' => 'CategoriesController@postAddCategory', 'as' => 'POST_ADD_CATEGORY']);
        Route::get('/show-categories', ['uses' => 'CategoriesController@getAllCategories', 'as' => 'GET_CATEGORY']);
        Route::get('/delete-category/{id}', ['uses' => 'CategoriesController@getDeleteCategory', 'as' => 'GET_DELETE_CATEGORY']);
        Route::get('/edit-category/{id}', ['uses' => 'CategoriesController@getEditCategory', 'as' => 'GET_EDIT_CATEGORY']);
        Route::post('/edit-category/{id}', ['uses' => 'CategoriesController@postEditCategory', 'as' => 'POST_EDIT_CATEGORY']);
        Route::get('/add-images/{id}', ['uses' => 'PhotosController@getAddImages', 'as' => 'GET_ADD_IMAGES']);
        Route::post('/add-images', ['uses' => 'PhotosController@postAddImages', 'as' => 'POST_ADD_Image']);
        Route::get('/all-images', ['uses' => 'PhotosController@getAllImages', 'as' => 'GET_All_IMAGES']);
        Route::get('/delete-image/{id}', ['uses' => 'PhotosController@getDeleteImage', 'as' => 'GET_DELETE_IMAGE']);
        Route::get('/edit-image/{id}', ['uses' => 'PhotosController@getEditImage', 'as' => 'GET_EDIT_IMAGE']);
        Route::post('/edit-image/{id}', ['uses' => 'PhotosController@postEditImage', 'as' => 'POST_EDIT_Image']);
        Route::get('/show-image/{id}', ['uses' => 'PhotosController@getShowImage', 'as' => 'GET_SINGLE_IMAGE']);
        Route::get('/add-package', ['uses' => 'PackageController@getAddPackage', 'as' => 'GET_ADD_PACKAGE']);
        Route::get('/edit-package/{id}', ['uses' => 'PackageController@getEditPackage', 'as' => 'GET_EDIT_PACKAGE']);
        Route::post('/edit-package/{id}', ['uses' => 'PackageController@postEditPackage', 'as' => 'POST_EDIT_PACKAGE']);
        Route::get('/all-photoPackges', ['uses' => 'PackageController@getAllTypes', 'as' => 'GET_ALL_PHOTO_TYPES']);
        Route::get('/delete/{id}', ['uses' => 'PackageController@getdeleteType', 'as' => 'GET_DELETE_PHOTO_TYPE']);
        Route::post('/add-package', ['uses' => 'PackageController@postAddPackage', 'as' => 'POST_ADD_Package']);
        Route::post('/add-photoType', ['uses' => 'PackageController@postAddtype', 'as' => 'POST_ADD_PHOTO_TYPE']);
        Route::get('/all-package', ['uses' => 'PackageController@getAllPackages', 'as' => 'GET_ALL_PACKAGE']);
        Route::get('/add-photo-type/{id}', ['uses' => 'PackageController@getAddType', 'as' => 'GET_ADD_PHOTO_TYPE']);
        Route::get('/delete-package/{id}', ['uses' => 'PackageController@getDeletePackages', 'as' => 'GET_DELETE_PACKAGE']);
        Route::get('/add-customer', ['uses' => 'CustomersController@getAddCustomer', 'as' => 'GET_ADD_CUSTOMER']);
        Route::post('/add-customer', ['uses' => 'CustomersController@postAddCustomer', 'as' => 'POST_ADD_CUSTOMER']);
        Route::get('/all-customer', ['uses' => 'CustomersController@getAllCustomers', 'as' => 'GET_ALL_CUSTOMER']);
        Route::get('/show-customer/{id}', ['uses' => 'CustomersController@getCustomer', 'as' => 'GET_SHOW_CUSTOMER']);
        Route::get('/delete-customer/{id}', ['uses' => 'CustomersController@deleteCustomer', 'as' => 'GET_DELETE_CUSTOMER']);
        Route::get('/edit-customer/{id}', ['uses' => 'CustomersController@getEditCustomer', 'as' => 'GET_EDIT_CUSTOMER']);
        Route::post('/edit-customer/{id}', ['uses' => 'CustomersController@postEditCustomer', 'as' => 'POST_EDIT_CUSTOMER']);
        Route::get('/orders', ['uses' => 'OrdersController@getAllOrders', 'as' => 'GET_ALL_ORDERS']);
        Route::get('/deleteOrder/{id}', ['uses' => 'OrdersController@deleteOrder', 'as' => 'GET_DELETE_ORDER']);
        Route::get('/sendMAil/{id}', ['uses' => 'OrdersController@getSendMail', 'as' => 'GET_SEND_MAIL']);
        Route::post('/sendMAil', ['uses' => 'OrdersController@postSendMail', 'as' => 'POST_SEND_MAIL']);
        Route::get('/show-setting', ['uses' => 'SettingsController@getAddSetting', 'as' => 'GET_SETTING']);
        Route::get('/show-settings', ['uses' => 'SettingsController@getSetting', 'as' => 'SHOW_SETTING']);
        Route::post('/show-setting', ['uses' => 'SettingsController@postAddSetting', 'as' => 'POST_ADD_SETTING']);
        Route::get('/edit-setting/{id}', ['uses' => 'SettingsController@getEditSetting', 'as' => 'GET_EDIT_SETTING']);
        Route::post('/edit-setting/{id}', ['uses' => 'SettingsController@postEditSetting', 'as' => 'POST_EDIT_SETTING']);
        Route::get('/add-subpackage/{id}', ['uses' => 'SubPackagesController@getAddSubPackage', 'as' => 'GET_ADD_SUB_PACKAGE']);
        Route::post('/add-subpackage', ['uses' => 'SubPackagesController@postAddSubPackage', 'as' => 'POST_ADD_SUB_Package']);
        Route::get('/all-subPackages', ['uses' => 'SubPackagesController@getAllSubPackages', 'as' => 'GET_ALL_SUB_PACKAGES']);
        Route::get('/get_delete_subPackage/{id}', ['uses' => 'SubPackagesController@getdeleteSubPackages', 'as' => 'GET_DELETE_SUB_PACKAGE']);
        Route::get('/allSubscribers', ['uses' => 'SubscribersController@getAllSubscribers', 'as' => 'GET_ALL_SUBSCRIBERS']);
        Route::get('/delete-sub/{id}', ['uses' => 'SubscribersController@deleteSubscriber', 'as' => 'GET_DELETE_SUBSCRIBER']);
        Route::get('/allMails', ['uses' => 'OrdersController@getAllMails', 'as' => 'GET_ALL_MAILS']);
        Route::get('/deleteMail/{id}', ['uses' => 'OrdersController@deleteMail', 'as' => 'GET_DELETE_EMAIL']);
    });
});
//                         User View Routes                                     //

Route::get('/', ['uses' => 'HomeController@getIndex', 'as' => 'Home']);
Route::get('/about', ['uses' => 'HomeController@getAbout', 'as' => 'About']);
//                      Language Changer                    //

Route::get('/language/{lang}', 'HomeController@changeLanguage')->name('CHANGE_LANGUAGE')->middleware('lang');


Route::get('/gallery', ['uses' => 'HomeController@getGallery', 'as' => 'GET_GALLERY']);
Route::get('/gallery/{id}', 'HomeController@showGallery');
Route::get('/services', ['uses' => 'HomeController@getService', 'as' => 'Services']);
Route::get('/contact', ['uses' => 'HomeController@getContact', 'as' => 'GET_CONTACT']);
Route::post('/contact', ['uses' => 'HomeController@postOrder', 'as' => 'POST_USER_ORDER']);
Route::post('/subscriber', ['uses' => 'HomeController@postSubsriber', 'as' => 'POST_SUBSCRIBER']);