<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function User(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function Category(){
        return $this->belongsTo(Category::class,'cat_id','id');
    }
}
