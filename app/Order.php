<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=[
      'name','email','activity','phone','photos_numbers','order_file','order_image','sub_packs','sub_packs_else',
    ];
}
