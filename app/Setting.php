<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable=[
        'top_logo',
        'background_image',
        'phone',
        'facebook',
        'twitter',
        'email',
        'instagram',
        'youtube',
        'pinterest',
        'behance',
        'vimo',
        'company_profile',
        'company_name_en',
        'company_name_ar',
        'SliderBackground','summary_en','summary_ar','service_en','service_ar'
    ];
}
