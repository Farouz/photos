<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable=[
      'title_en','title_ar'
    ];
    public function SubPackage(){
        return $this->hasMany(Subscriber::class,'package_id','id');
    }
    public function PhotoTypes(){
        return $this->hasMany(PhotoType::class,'package_id','id');
    }
}
