<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubPackage extends Model
{
    public function Package(){
        return $this->belongsTo(Package::class,'package_id','id');
    }
    protected $fillable=[
      'title_en','title_ar','package_id'
    ];
}
