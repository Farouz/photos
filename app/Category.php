<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=[
      'title_en','title_ar','image'
    ];

    public function photos(){
        return $this->hasMany(Photo::class,'cat_id','id');
    }
}
