<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class SubscribersController extends Controller
{
public  function getAllSubscribers(){
    $subCount=Subscriber::count();
    $subscribers=Subscriber::all();
    return view('dashboard.Subscriber.AllSub',compact('subscribers','subCount'));
}
public function deleteSubscriber($id){
    Subscriber::destroy($id);
    return back()->with('error','Subscriber Deleted');
}
}
