<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\Order;
use App\Photo;
use App\Package;
use App\SubPackage;
use App\PhotoType;
use App\Subscriber;
use App\Setting;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App;
use Lang;

class HomeController extends Controller
{
    public function getIndex()
    {
        $settings= Setting::all();
        return view('User.index',compact('settings'));
    }

    public function getAbout()
    {
        $settings= Setting::all();
        $customers = Customer::all();
        return view('User.about',compact('settings','customers'));
    }

    public function getGallery()
    {
        $settings= Setting::all();
        $categories = Category::all();
        return view('User.category.allCategories', compact('categories','settings'));
    }

    public function ShowGallery($id)
    {
        $settings= Setting::all();

        $category = Category::findOrFail($id);
        $images = Photo::where('cat_id', $category->id)->get();
        return view('User.category.allImages', compact('category', 'images','settings'));
    }

    public function getService()
    {
        $settings= Setting::all();
        return view('User.Service',compact('settings'));
    }

    public function getContact()
    {
        $settings= Setting::all();

        $packages = Package::all();
        $subPackages = SubPackage::all();
        $types = PhotoType::all();
        return view('User.contact', compact('packages', 'subPackages', 'types','settings'));
    }

    //Translation
    public function changeLanguage($lang)
    {
        if ($lang == 'en' || $lang == 'ar') {
            if (!\Session::has('language')) {
                \Session::put('language', $lang);
            } else {
                \Session::put('language', $lang);
            }
            return back();
        } else {
            return redirect()->route('Home');
        }
    }

    public function postOrder(Request $request)
    {
        $this->validate($request, [
            'packages'=>'required|array|min:1',
            'name' => 'required|regex:/^([^0-9]*)$/',
            'activity' => 'required|regex:/^([^0-9]*)$/',
            'phone' => 'required|numeric|min:10',
            'email' => 'required|email',
            'order_file' => 'mimes:jpeg,bmp,png,pdf,docx',
            'order_image' => 'required'
        ]);

        foreach ($request->input('packages') as $package){

            $order = new Order();
            $order->name = $request->name;
            $order->activity = $request->activity;
            $order->email = $request->email;
            $order->phone = $request->phone;
            $elsePackage=$request->input('sub_packs_else');
            $order->sub_packs_else = $elsePackage[$package];
            $photoNumbers=$request->input('photos_numbers');
            $order->photos_numbers=$photoNumbers[$package];
            $subPacks=$request->input('sub_packs');
            if (isset($subPacks[$package]) && count($subPacks[$package]) > 0) {
                foreach ($subPacks[$package] as $pck) {
                    $packs[] = $pck;
                }
                $pcks = implode(' ', $packs);
                $order->service = $pcks;
                $order->save();
            } else {
                $order->service=null;
                $order->save();
            }
        }




        if ($request->has('order_image')) {
            foreach ($request->input('order_image') as $orImg) {
                $orImgs[] = $orImg;
            }
            $orImages = implode(' ', $orImgs);
            $order->order_image = $orImages;
            $order->save();
        }
        if ($request->hasFile('order_file')) {
            $order_file = md5($request->order_file->getClientOriginalName()) . '.' . $request->order_file->getClientOriginalExtension();
            $request->order_file->move(public_path('uploads/images'), $order_file);
            $order->order_file = $order_file;
            $order->save();
        }
        $order->save();
        return redirect()->route('GET_CONTACT')->with('success', 'Message Sent');
    }

    public function postSubsriber(Request $request){
        $this->validate($request,[
           'email'=>'required|email|unique:subscribers,email'
        ]);
        $subscriber=new Subscriber();
        $subscriber->email=$request->email;
        $subscriber->save();
        return back()->with('success','Email Sent');
    }
}
