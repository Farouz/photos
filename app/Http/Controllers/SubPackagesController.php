<?php

namespace App\Http\Controllers;

use App\Package;
use App\SubPackage;
use Illuminate\Http\Request;

class SubPackagesController extends Controller
{
    public function getAddSubPackage($id){
        $mainPackage=Package::findOrFail($id);
        return view('dashboard.Package.addSubPackage',compact('mainPackage'));
    }
    public function postAddSubPackage(Request $request){
        $this->validate($request,[
            'title_en'=>'required|regex:/^([^0-9]*)$/',
            'title_ar'=>'required|regex:/^([^0-9]*)$/',
        ]);
        $subPackage=new SubPackage();
        $subPackage->package_id=$request->package_id;
        $subPackage->title_en=$request->title_en;
        $subPackage->title_ar=$request->title_ar;
        $subPackage->save();
        return redirect()->route('GET_ALL_SUB_PACKAGES')->with('success','Sub Package Added !');
    }
    public function getAllSubPackages(){
        $subCounts=SubPackage::count();
        $subPackages=SubPackage::all();
        return view('dashboard.package.allSubPackages',compact('subPackages','subCounts'));
    }
    public function getdeleteSubPackages($id){
        SubPackage::destroy($id);
        return back()->with('error','Sub Package Deleted');
    }
}
