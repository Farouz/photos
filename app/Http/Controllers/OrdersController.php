<?php

namespace App\Http\Controllers;

use App\Email;
use App\Mail\MyMail;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;

class OrdersController extends Controller
{
    public function getAllOrders()
    {
        $orders = Order::all();
        $ordersCount = Order::count();
        return view('dashboard.Orders.allOrders', compact('orders', 'ordersCount'));
    }

    public function deleteOrder($id)
    {
        Order::destroy($id);
        return back();
    }

    public function getSendMail($id)
    {
        $order = Order::findOrFail($id);
        return view('dashboard.Orders.SendMail', compact('order'));
    }

    public function postSendMail(Request $request, Mailer $mailer)
    {
        $message = new Email();
        $message->title =strip_tags( $request->input('title'));
        $message->customer_mail = $request->input('email');
        $message->save();
        $mailer->to($request->input('email'))->send(new MyMail(strip_tags($request->input('title')), $request->input('message'), $request->input('email')));
        return redirect()->route('GET_ALL_ORDERS')->with('success', 'Email Sent ');
    }
    public function getAllMails(){
        $messages=Email::all();
        return view('dashboard.Orders.AllMails',compact('messages'));
    }
    public function deleteMail($id){
        Email::destroy($id);
        return redirect()->back()->with('alert','Message Deleted');
    }
}
