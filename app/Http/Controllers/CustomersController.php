<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function getAddCustomer(){
        return view('dashboard.customers.addCustomers');
    }
    public function postAddCustomer(Request $request){
        $this->validate($request,[
            'title_en'=>'required|regex:/^([^0-9]*)$/',
            'title_ar'=>'required|regex:/^([^0-9]*)$/',
            'image'=>'required|mimes:jpeg,bmp,png'
        ]);
        if ($request->hasFile('image')){
            $image_name=md5($request->image->getClientOriginalName()).'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/images'),$image_name);
            $customer = new Customer();
            $customer->title_en=$request->title_en;
            $customer->title_ar=$request->title_ar;
            $customer->image=$image_name;
            $customer->save();
            return redirect()->route('GET_ALL_CUSTOMER')->with('success','Customer Added Successfully');
        }
    }
    public function getAllCustomers(){
        $custCount=Customer::count();
        $customers=Customer::all();
        return view('dashboard.customers.showCustomers',compact('custCount','customers'));
    }

    public function deleteCustomer($id){
        Customer::destroy($id);
        return redirect()->route('GET_ALL_CUSTOMER')->with('error','Customer Deleted');
    }
    public  function  getEditCustomer($id){
        $customer=Customer::findOrFail($id);
        return view('dashboard.customers.editProfile',compact('customer'));
    }
    public function postEditCustomer(Request $request,$id){
        $this->validate($request,[
            'title_en'=>'regex:/^([^0-9]*)$/',
            'title_ar'=>'regex:/^([^0-9]*)$/',
            'image'=>'mimes:jpeg,bmp,png'
        ]);
        $customer=Customer::findOrFail($id);
        if ($request->hasFile('image')){
            $image_name=md5($request->image->getClientOriginalName()).'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/images'),$image_name);
            $customer->image=$image_name;
            $customer->save();
        }else{
            $old_image=$customer->image;
            $customer->image=$old_image;
            $customer->save();
        }
        if ($request->has('title_en')){
            $customer->title_en=$request->title_en;
            $customer->save();
        }else
        {
            $customer->title_en=$request->title_en;
            $customer->save();
        }
        if ($request->has('title_ar')){
            $customer->title_ar=$request->title_ar;
            $customer->save();
        }else
        {
            $customer->title_ar=$request->title_ar;
            $customer->save();
        }
        return redirect()->route('GET_ALL_CUSTOMER')->with('success','Customer Edited Successfully');
    }

}
