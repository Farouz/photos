<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function getAddCategory(){
        return view('dashboard.category.addCategory');
    }
    public function postAddCategory(Request $request){
        $this->validate($request,[
            'title_en'=> 'required|regex:/^([^0-9]*)$/',
            'title_ar'=> 'required|regex:/^([^0-9]*)$/',
            'image'=>'required|mimes:jpeg,bmp,png'
        ]);
        if ($request->hasFile('image')){
            $image_name = md5($request->image->getClientOriginalName()) .'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/images'),$image_name);
            $category = new Category();
            $category->title_en=$request->title_en;
            $category->title_ar=$request->title_ar;
            $category->image=$image_name;
            $category->save();
            return redirect()->route('GET_CATEGORY')->with('success','Category Added Successfully');
        }
    }
    public function getAllCategories(){
        $catCount=Category::count();
        $categories=Category::all();
        return view('dashboard.category.allCategories',compact('catCount','categories'));
    }
    public function getDeleteCategory($id){
        Category::destroy($id);
        return redirect()->route('GET_CATEGORY')->with('error','Category Deleted');
    }
    public function getEditCategory($id){
        $category=Category::findOrFail($id);
        return view('dashboard.category.editCategory',compact('category'));
    }
    public function postEditCategory(Request $request,$id){
        $this->validate($request,[
            'title_en'=> 'regex:/^([^0-9]*)$/',
            'title_ar'=> 'regex:/^([^0-9]*)$/',
            'image'=>'mimes:jpeg,bmp,png'
        ]);
        $category = Category::findOrFail($id);
        if ($request->hasFile('image')) {
            $image_name = md5($request->image->getClientOriginalName()) . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/images'), $image_name);
            $category->image = $image_name;
            $category->save();
        }else {
            $old_image=$category->image;
            $category->image=$old_image;
            $category->save();
        }
        if($request->has('title_en')){
            $category->title_en=$request->title_en;
            $category->save();
        }
        else{
            $category->title_en=$request->title_en;
            $category->save();
        }
            if ($request->has('title_ar')){
                $category->title_ar = $request->title_ar;
                $category->save();
            }
        else{
                $category->title_ar=$request->title_ar;
                $category->save();
        }

            return redirect()->route('GET_CATEGORY')->with('success','Category Edited Successfully');
        }
}
