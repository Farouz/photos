<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\Email;
use App\Order;
use App\Package;
use App\Photo;
use App\Subscriber;
use App\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function getDashboard(){
        $catCount=Category::count();
        $imgCount=Photo::count();
        $ordersCount=Order::count();
        $subCount=Subscriber::count();
        $packCount=Package::count();
        $custCount=Customer::count();
        $admins=User::count();
        $messagesCount=Email::count();
        return view('dashboard.index',compact('catCount','imgCount','ordersCount','subCount','packCount','custCount','admins','messagesCount'));
    }
    public function getLogin(){
        return view('dashboard.admin.adminLogin');
    }
    public function postLogin(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:6'
        ]);
        if(!  auth()->attempt(\request(['email','password']))){
            return back()->withErrors([
                'message'=>'Email or password not correct'
            ]);
        };
        return redirect()->route('GET_ADMIN_DASHBOARD');
    }
    public function getAllAdmins(){
        $admins=User::all();
        return view('dashboard.admin.allAdmins',compact('admins'));
    }
    public function deleteAdmin($id){
        User::destroy($id);
        return back();
    }

    public function getAddAdmin(){
        return view('dashboard.admin.AddAdmin');
    }
    public function postAddAdmin(Request $request){
        $this->validate($request,[
           'name'=>'required|regex:/^([^0-9]*)$/',
           'email'=>'required|email|unique:orders,email',
           'password'=>'required|min:5|max:10'
        ]);
        $user=new  User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->save();
        return redirect()->route('GET_ALL_ADMINS')->with('success','Admin Added successful');
    }
    public function getSignOut(){
        auth()->logout();
        return redirect()->route('GET_ADMIN_LOGIN');
    }
}
