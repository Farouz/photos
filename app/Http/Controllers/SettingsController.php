<?php

namespace App\Http\Controllers;

use App\Package;
use App\Setting;
use App\SubPackage;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function getAddSetting()
    {
        return view('dashboard.settings.addSettings');
    }

    public function postAddSetting(Request $request)
    {

        $this->validate($request, ['company_name_en' => 'required|regex:/^([^0-9]*)$/', 'company_name_ar' => 'required|regex:/^([^0-9]*)$/', 'top_logo' => 'required|image:jpg,png|dimensions:min_width=50,min_height=45', 'background_image' => 'required|image:jpg,png|max:5000', 'company_profile' => 'required|mimes:pdf,docs,docx', 'service_en' => 'required', 'service_ar' => 'required', 'summary_en' => 'required', 'summary_ar' => 'required', 'phone' => 'required', 'facebook' => 'required|regex:/^([^0-9]*)$/', 'twitter' => 'required|regex:/^([^0-9]*)$/', 'youtube' => 'required|regex:/^([^0-9]*)$/', 'pinterest' => 'required|regex:/^([^0-9]*)$/', 'behance' => 'required|regex:/^([^0-9]*)$/', 'vimo' => 'required|regex:/^([^0-9]*)$/', 'instagram' => 'required|regex:/^([^0-9]*)$/', 'email' => 'required|email|regex:/^([^0-9]*)$/',]);
        $setting = new Setting();
        if ($request->hasFile('top_logo')) {
            $top_logo = md5($request->top_logo->getClientOriginalName()) . '.' . $request->top_logo->getClientOriginalExtension();
            $request->top_logo->move(public_path('uploads/images'), $top_logo);
            $setting->top_logo = $top_logo;
        }

        if ($request->hasFile('background_image')) {
            $background_image = md5($request->background_image->getClientOriginalName()) . '.' . $request->background_image->getClientOriginalExtension();
            $request->background_image->move(public_path('uploads/images'), $background_image);
            $setting->background_image = $background_image;
        }

       if ($request->hasFile('sliderBackground')){
            foreach ($request->file('sliderBackground') as $slider){
                $slider_name=md5($slider->getClientOriginalName()).'.'.$slider->getClientOriginalExtension();
                $slider->move(public_path('uploads/images/'),$slider_name);
                $sliders[]=$slider_name;
            }
            $sliders=implode(" ",$sliders);
           $setting->SliderBackground=$sliders;
       }

        if ($request->hasFile('company_profile')) {
            $company_profile = md5($request->company_profile->getClientOriginalName()) . '.' . $request->company_profile->getClientOriginalExtension();
            $request->company_profile->move(public_path('uploads/Files'), $company_profile);
            $setting->company_profile = $company_profile;
        }
        $setting->company_name_en = $request->company_name_en;
        $setting->service_en=strip_tags($request->service_en);
        $setting->service_ar=strip_tags($request->service_ar);
        $setting->summary_en=strip_tags($request->summary_en);
        $setting->summary_ar=strip_tags($request->summary_ar);
        $setting->company_name_ar = $request->company_name_ar;
        $setting->phone = $request->phone;
        $setting->facebook = $request->facebook;
        $setting->twitter = $request->twitter;
        $setting->youtube = $request->youtube;
        $setting->pinterest = $request->pinterest;
        $setting->vimo = $request->vimo;
        $setting->behance = $request->behance;
        $setting->email = $request->email;
        $setting->instagram = $request->instagram;
        $setting->save();
        return redirect()->route('SHOW_SETTING')->with('success','Done');
    }

    public function getSetting()
    {
        $settings = Setting::all();
        return view('dashboard.settings.showSetting', compact('settings'));
    }

    public function getEditSetting($id)
    {
        $setting = Setting::findOrFail($id);
        return view('dashboard.settings.editSetting', compact('setting'));
    }

    public function postEditSetting($id, Request $request)
    {

        $setting = Setting::findOrFail($id);
        $this->validate($request, ['company_name_en' => 'required|regex:/^([^0-9]*)$/',
            'company_name_ar' => 'required|regex:/^([^0-9]*)$/',
            'top_logo' => 'required|image:jpg,png|dimensions:min_width=50,min_height=45',
            'background_image' => 'required|image:jpg,png|max:5000',
            'company_profile' => 'required|mimes:pdf,docs,docx',
            'service_en'=>'required',
            'service_ar'=>'required',
            'summary_en'=>'required',
            'summary_ar'=>'required',
            'phone' => 'required',
            'facebook' => 'required|regex:/^([^0-9]*)$/', 'twitter' => 'required|regex:/^([^0-9]*)$/', 'youtube' => 'required|regex:/^([^0-9]*)$/', 'pinterest' => 'required|regex:/^([^0-9]*)$/', 'behance' => 'required|regex:/^([^0-9]*)$/', 'vimo' => 'required|regex:/^([^0-9]*)$/', 'instagram' => 'required|regex:/^([^0-9]*)$/', 'email' => 'required|email|regex:/^([^0-9]*)$/']);
        if ($request->hasFile('top_logo')) {
            $top_logo = md5($request->top_logo->getClientOriginalName()) . '.' . $request->top_logo->getClientOriginalExtension();
            $request->top_logo->move(public_path('uploads/images'), $top_logo);
            $setting->top_logo = $top_logo;
        }
        else{
            $setting->top_logo=$request->top_logo;
        }
        if ($request->hasFile('background_image')) {
            $background_image = md5($request->background_image->getClientOriginalName()) . '.' . $request->background_image->getClientOriginalExtension();
            $request->background_image->move(public_path('uploads/images'), $background_image);
            $setting->background_image = $background_image;
        } else {
            $setting->background_image = $request->background_image;
        }
        if ($request->hasFile('company_profile')) {
            $company_profile = md5($request->company_profile->getClientOriginalName()) . '.' . $request->company_profile->getClientOriginalExtension();
            $request->company_profile->move(public_path('uploads/Files'), $company_profile);
            $setting->company_profile = $company_profile;
        } else {
            $setting->company_profile = $request->company_profile;
        }
        if ($request->hasFile('sliderBackground')){
            foreach ($request->file('sliderBackground') as $slider){
                $slider_name=md5($slider->getClientOriginalName()).'.'.$slider->getClientOriginalExtension();
                $slider->move(public_path('uploads/images/'),$slider_name);
                $sliders[]=$slider_name;
            }
            $sliders=implode(" ",$sliders);
            $setting->SliderBackground=$sliders;
        }
        else{
            $oldSlider=$setting->SliderBackground;
            $setting->SliderBackground=$oldSlider;
        }

        if ($request->has('company_name_en')) {
            $setting->company_name_en = $request->company_name_en;

        } else {
            $setting->company_name_en = $request->company_name_en;

        }
        if ($request->has('company_name_ar')) {
            $setting->company_name_ar = $request->company_name_ar;

        } else {
            $setting->company_name_ar = $request->company_name_ar;

        }
        if ($request->has('phone')) {
            $setting->phone = $request->phone;

        } else {
            $setting->phone = $request->phone;
        }
        if ($request->has('email')) {
            $setting->email = $request->email;
        } else {
            $setting->email = $request->email;
        }
        if ($request->has('facebook')) {
            $setting->facebook = $request->facebook;
        } else {
            $setting->facebook = $request->facebook;
        }
        if ($request->has('twitter')) {
            $setting->twitter = $request->twitter;
        } else {
            $setting->twitter = $request->twitter;
        }
        if ($request->has('youtube')) {
            $setting->youtube = $request->youtube;
        } else {
            $setting->youtube = $request->youtube;
        }
         if ($request->has('instagram')) {
            $setting->instagram = $request->instagram;
        } else {
            $setting->instagram = $request->instagram;
        }
        if ($request->has('pinterest')) {
            $setting->pinterest = $request->pinterest;
        } else {
            $setting->pinterest = $request->pinterest;
        }
         if ($request->has('behance')) {
            $setting->behance = $request->behance;
        } else {
            $setting->behance = $request->behance;
        }
        if ($request->has('vimo')) {
            $setting->vimo = $request->vimo;
        } else {
            $setting->vimo = $request->vimo;
        }
        if ($request->has('service_ar')){
            $setting->service_ar=$request->service_ar;
        }else{
            $setting->service_ar=$request->service_ar;

        }if ($request->has('service_en')){
            $setting->service_en=$request->service_en;
        }else{
            $setting->service_en=$request->service_en;

        }if ($request->has('summary_en')){
            $setting->summary_en=$request->summary_en;
        }else{
            $setting->summary_en=$request->summary_en;

        }if ($request->has('summary_ar')){
            $setting->summary_ar=$request->summary_ar;
        }else{
            $setting->summary_ar=$request->summary_ar;
        }
        $setting->save();
        return redirect()->route('SHOW_SETTING')->with('success','Setting Modified');
    }


}
