<?php

namespace App\Http\Controllers;

use App\Category;
use App\Photo;
use Illuminate\Http\Request;

class PhotosController extends Controller
{

    public function getAddImages($id){
        $category=Category::findOrFail($id);
        return view('dashboard.Images.addImage',compact('category'));
    }
    public function postAddImages(Request $request){
        $this->validate($request,[
            'title_en'=> 'required|regex:/^([^0-9]*)$/',
            'title_ar'=> 'required|regex:/^([^0-9]*)$/',
            'image'=>'required|mimes:jpeg,bmp,png'
        ]);
        if ($request->hasFile('image')){
            $image_name = md5($request->image->getClientOriginalName()) .'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/images'),$image_name);
            $photo = new Photo();
            $photo->title_en=$request->title_en;
            $photo->title_ar=$request->title_ar;
            $photo->image=$image_name;
            $photo->cat_id=$request->cat_id;
            $photo->user_id=$request->user_id;
            $photo->save();
            return redirect()->route('GET_All_IMAGES')->with('success','Images Added Successfully');
        }
    }
    public function getAllImages(){
        $images=Photo::all();

        $imgCount=Photo::count();
        return view('dashboard.Images.allImages',compact('images','imgCount'));
    }
    public function getDeleteImage($id){
        Photo::destroy($id);
        return redirect()->route('GET_All_IMAGES')->with('error','Image Deleted');
    }
    public function getEditImage($id){
        $image=Photo::findOrFail($id);
        return view('dashboard.Images.editImage',compact('image'));
    }
    public function postEditImage(Request $request , $id ){
        $this->validate($request,[
            'title_en'=> 'regex:/^([^0-9]*)$/',
            'title_ar'=> 'regex:/^([^0-9]*)$/',
            'image'=>'mimes:jpeg,bmp,png'
        ]);
        $image=Photo::findOrFail($id);
        if ($request->hasFile('image')) {
            $image_name = md5($request->image->getClientOriginalName()) . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/images'), $image_name);
            $image->image = $image_name;
            $image->save();
        }else{
            $old_image=$image->image;
            $image->image=$old_image;
            $image->save();
        }
        if ($request->has('title_en')){
            $image->title_en=$request->title_en;
            $image->save();
        }
        else{
            $image->title_en=$request->title_en;
            $image->save();
        }
        return redirect()->route('GET_All_IMAGES')->with('success','Image Edited Successfully');
    }

}
