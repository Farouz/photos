<?php

namespace App\Http\Controllers;

use App\Package;
use App\Photo;
use App\PhotoType;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function getAddPackage(){
        return view('dashboard.Package.addPackage');
    }
    public function postAddPackage(Request $request){
        $this->validate($request,[
            'title_en'=> 'required|regex:/^([^0-9]*)$/',
            'title_ar'=> 'required|regex:/^([^0-9]*)$/',

        ]);
        $package=new Package();
        $package->title_en=$request->title_en;
        $package->title_ar=$request->title_ar;

        $package->save();
        return redirect()->route('GET_ALL_PACKAGE')->with('success','Package Added Successfully');
    }
    public function getAllPackages(){
        $packages=Package::all();
        $packagesCount=Package::count();
        return view('dashboard.Package.allPackages',compact('packages','packagesCount'));
    }
    public function getEditPackage($id){
        $package=Package::findOrFail($id);
        return view('dashboard.Package.editPackage',compact('package'));
    }
    public function postEditPackage($id,Request $request){
        $package=Package::findOrFail($id);
        if ($request->has('title_en')){
            $package->title_en=$request->title_en;
            $package->save();
        }else{
            $package->title_en=$request->title_en;
            $package->save();
        }
        if ($request->has('title_ar')){
            $package->title_ar=$request->title_ar;
            $package->save();
        }else{
            $package->title_ar=$request->title_ar;
            $package->save();
        }
        return redirect()->route('GET_ALL_PACKAGE')->with('success','Package Updated Successfully');
    }
    public function getDeletePackages($id){
        Package::destroy($id);
        return back()->with('error','Package Deleted');
    }
    public function getAddType($id){
        $package=Package::findOrFail($id);
        return view('dashboard.Package.addPhotoType',compact('package'));
    }
    public function postAddtype(Request $request){
        $this->validate($request,[
            'title_en'=> 'required|regex:/^([^0-9]*)$/',
            'title_ar'=> 'required|regex:/^([^0-9]*)$/',
        ]);
        $type=new PhotoType();
        $type->package_id=$request->package_id;
        $type->title_en=$request->title_en;
        $type->title_ar=$request->title_ar;
        $type->save();
        return redirect()->route('GET_ALL_PHOTO_TYPES')->with('success','Photo Type Added ');
    }
    public function getAllTypes(){
        $types=PhotoType::all();
        $typesCount=PhotoType::count();
        return view('dashboard.Package.allPhotoTypes',compact('types','typesCount'));
    }
    public function getdeleteType($id){
        PhotoType::destroy($id);
        return back()->with('error','Type Deleted');
    }
}
