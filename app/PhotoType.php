<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoType extends Model
{
    protected $fillable=[
      'title_en','title_ar','package_id'
    ];
    public function Package(){
        return $this->belongsTo(Package::class,'package_id','id');
    }
}
